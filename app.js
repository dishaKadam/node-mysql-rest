const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const logger = require('morgan');
const mySql = require('mysql');
const config = require('./config');


const employeeModule = config.routeSlug.employee;

const app = express();

app.use(cors());
app.use(logger('dev'));
app.use(function(request,response,next){

    const con = mySql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'root123',
    database : 'emp_eval'
    });

    con.connect((err) => {
    if(err){
        console.log('Error connecting to Db');
        return;
    }
    console.log('Connection established');
    });

    request.mysqlConnection = con;
    next();
});
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());


/*  Routes  */
const employeeRoutes = require('./api/routes/employee-router');

app.use("/" + employeeModule, employeeRoutes);


/*  Handling 404  */
app.use((request, response, next) => {
    const error = new Error("Not Found");
    error.status = "404";
    next(error);
})

/*  Handling Error or 500  */
app.use((error, request, response, next) => {

    const status = error.status || 500;

    return response.status(status)
        .json({
            error: {
                message: error.message
            }
        });
});

module.exports = app;