const http = require('http');
const app = require('./app');
const config = require('./config');

const port = config.port;
const host = config.host;

http.createServer(app).listen(port, host);

console.log("Running app on port : ", port);