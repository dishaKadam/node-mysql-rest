const express = require('express');
const router = express.Router();
const employeeController = require('../controllers/employee-controller');
const config = require('../../config');

/* Route - Employee Add */
router.post('/', employeeController.store);

/* Route - Get Specific Data using Id */
router.get('/:employeeID', employeeController.get);

/* Route - Update Employee */
router.put('/:employeeID', employeeController.update);

/* Route - Getting All Employees */
router.get('/', employeeController.getAll);

/* Route - Delete Employee */
router.delete('/:employeeID', employeeController.delete);

module.exports = router;