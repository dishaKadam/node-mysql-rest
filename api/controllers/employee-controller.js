const config = require('../../config');
const moduleName = config.routeSlug.employee;

/*  Possible Routes  */
const RequestUrl = function () {
    return {
        'list': {
            "url": config.baseUrl + moduleName,
            "method": "GET",
        },
        'details': {
            "url": config.baseUrl + moduleName,
            "method": "GET",
        },
        'create': {
            "url": config.baseUrl + moduleName,
            "method": "POST",
        },
        'update': {
            "url": config.baseUrl + moduleName,
            "method": "PUT",
        },
        'delete': {
            "url": config.baseUrl + moduleName,
            "method": "DELETE",
        },
    };
}

let Controller = {};

/* Controller - Employee Add */ 
Controller.store = (request, response, next) => {

    /* validation */
    // const validate = Joi.validate(request.body, {
    //     name: Joi.string().required(),
    //     category: Joi.string().required(),
    //     description: Joi.any(),
    //     image: Joi.any()
    // });

    // if(validate.error){
    //     return response.status(500)
    //                     .json({
    //                         message: validate.error.details[0].message
    //                     });
    // }

    // let employee = request.params.employee;
    let employee = {
                 'employee_id': request.body.employee_id,
                'employee_name': request.body.employee_name,
                'employee_role': request.body.employee_role
    }
    //  const employee = new any({
           
    //         });

 
    if (!employee) {
        return response.status(400).send({ error:true, message: 'Please provide employee' });
    }
 
    request.mysqlConnection.query("INSERT INTO employee SET ? ", { employee_id : employee.employee_id, employee_name : employee.employee_name, employee_role : employee.employee_role }, function (error, results, fields) {
        request.mysqlConnection.end();
        if (error) {
	  		response.send(JSON.stringify({"status": 500, "error": error, "response": null})); 
	  		//If there is error, we send the error in the error section with 500 status
	  	} else{
            response.send(JSON.stringify({"status": 200, "error": null, "response": results}));
          }
        
    });


   

};

/* Controller - Get Specific Data using Id */
Controller.get = (request, response, next) => {
   
};

/* Controller - Update Employee */
Controller.update = (request, response, next) => {
    // const id = request.params.employeeID;

    // /* validation */
    // const validate = Joi.validate(request.body,{
    //     name: Joi.string().required(),
    //     category: Joi.string().required(),
    //     description: Joi.any(),
    //     image: Joi.any()
    // })

    // if(validate.error){
    //     return response.status(500)
    //                     .json({
    //                         message: validate.error.details[0].message
    //                     });
    // }

   
};

/* Controller - Getting All Employee */
Controller.getAll = (request, response, next) => {
   

	request.mysqlConnection.query('SELECT * from employee', function (error, results, fields) {
	  	request.mysqlConnection.end();
        if(error){
	  		response.send(JSON.stringify({"status": 500, "error": error, "response": null})); 
	  		//If there is error, we send the error in the error section with 500 status
	  	} else {
  			response.send(JSON.stringify({"status": 200, "error": null, "response": results}));
  			//If there is no error, all is good and response is 200OK.
	  	}
  	});

};

/* Controller - Delete Employee */
Controller.delete = (request, response, next) => {
    // const id = request.params.employeeID;

    
};


module.exports = Controller;